import { Injectable } from '@angular/core';
import { elementAt } from 'rxjs';
import { DatosPersonales } from '../interfaces/citas';

@Injectable({
  providedIn: 'root'
})
export class CitaService {
  LISTA: DatosPersonales[] = [];
  constructor() { }

  getCitas(){
    if(localStorage.getItem('citasLis')===null){
      return this.LISTA;
    }else{
      this.LISTA=JSON.parse(localStorage.getItem('citasLis')||"[]")
      return this.LISTA
  }
  }
  agregarCita(cita:DatosPersonales){
    this.LISTA.push(cita);
    let citaLis: DatosPersonales[] = [];
    if(localStorage.getItem('citasLis')===null){
      citaLis.push(cita)
      localStorage.setItem('citasLis',JSON.stringify(citaLis))
    }
    else{
      citaLis=JSON.parse(localStorage.getItem('citasLis') || "[]")
      citaLis.push(cita);
      localStorage.setItem('citasLis',JSON.stringify(citaLis));
  }
  }
  eliminarCita(nombre:string){
    this.LISTA=this.LISTA.filter(data => data.nombre !== nombre)
    console.log(this.LISTA);
    localStorage.setItem('citasLis',JSON.stringify(this.LISTA));
    this.getCitas();
    
   
  }


   modificarCita(names:DatosPersonales){
    this.eliminarCita(names.nombre);
    this.agregarCita(names);
  }

  buscarCita(id:string):DatosPersonales{
    return this.LISTA.find(element => element.nombre === id) || {} as DatosPersonales;
  }
}
