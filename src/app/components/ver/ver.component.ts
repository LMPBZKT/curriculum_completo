import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CitaService } from 'src/app/servide/cita.service';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.component.html',
  styleUrls: ['./ver.component.css']
})
export class VerComponent implements OnInit {
  form!: FormGroup;
  constructor(private fb:FormBuilder,
    private _citaService:CitaService
    ,private router:Router,
    private activateRouted:ActivatedRoute) 
    {
      this.activateRouted.params.subscribe(params =>{
        const id = params['id'];
        console.log(id);

      const nombre =this._citaService.buscarCita(id);
      console.log(nombre);
      
      if (Object.keys(nombre).length === 0) {
        // volvemos a la ruta usuarios si no hay datos en el json
        this.router.navigate(['/formulario']);
      }
        
      this.form=this.fb.group({
        nombre:['',[Validators.required,Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
        apellido:['',[Validators.required,Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
        correo:['',[Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
        celular:['',[Validators.required,Validators.pattern(/^([0-9])*$/)]],
        fecha:['',[Validators.required]],
        hora:['',[Validators.required]],
        descripcion:['',[Validators.required]]
      });

      this.form.patchValue({
        nombre:nombre.nombre,
        apellido:nombre.apellido,
        correo:nombre.correo,
        celular:nombre.celular,
        fecha:nombre.fecha,
        hora:nombre.hora,
        descripcion:nombre.descripcion
      });
     })
    }
  ngOnInit(): void {
  }
  
  Volver(): void {
    this.router.navigate(['/formulario'])
  }
}
