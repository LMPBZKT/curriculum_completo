import { Component, OnInit,ViewChild } from '@angular/core';
import { DatosPersonales } from 'src/app/interfaces/citas';
import { MatTableDataSource } from '@angular/material/table';
import { CitaService } from 'src/app/servide/cita.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  LISTA_CITAS: DatosPersonales[] = []

  //Para las columnas
  displayedColumns: string[] = ['nombre', 'apellido', 'correo', 'celular', 'fecha', 'hora', 'descripcion', 'acciones'];
  dataSource !: MatTableDataSource<any>;


  constructor(private _citasService: CitaService,
    private _snackbar: MatSnackBar,
    private router:Router) { }


  ngOnInit(): void {
    this.cargarCitas();
  }

  cargarCitas() {
    this.LISTA_CITAS = this._citasService.getCitas();
    this.dataSource = new MatTableDataSource(this.LISTA_CITAS)
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarCita(cita:string) {

    const opcion = confirm(`¿Esta seguro de eliminar el registro  ${cita}?`);

    if (opcion) {
      console.log(cita);
      this._citasService.eliminarCita(cita);
      this.cargarCitas();
      this._snackbar.open("El registro fue eliminado", "Aceptar", {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      })
    }
  }

  modificarCita(nombre:string){
    console.log(nombre);
    this.router.navigate(['/agregar', nombre]); 
  }

  ver(nombre:string):void{
    console.log(nombre);
    this.router.navigate(['/ver', nombre])
    
  }

}




