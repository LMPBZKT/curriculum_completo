import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formulario2',
  templateUrl: './formulario2.component.html',
  styleUrls: ['./formulario2.component.css']
})
export class Formulario2Component implements OnInit {
  forma!: FormGroup;
  registerSend = true

  constructor(private fb: FormBuilder) {
    this.crearFormulario();
    // this.cargarDataAlFomulario2();
    
  }


  ngOnInit(): void {
  }
  crearFormulario(): void {
    this.forma = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(4)]],
      apellido: ['', [Validators.required, Validators.minLength(4)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      mensaje: ['', [Validators.required, Validators.minLength(2)]]

    })
  };
  get nombreNoValido() {
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }

  get apellidoNoValido() {
    return this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.touched;
  }

  get correoNoValido() {
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
  }
  guardar(): void{
    if(!this.forma.valid){
      return;
    }

    this.registerSend = false
    setTimeout(() => {
      this.registerSend = true
    }, 2000);

    console.log(this.forma.value);
    //Reset del formulario
    this.LimpiarFomulario();
  }

  LimpiarFomulario(): void{
    //this.forma.reset();
    this.forma.reset({
      nombre: ''
    });


}
cargarDataAlFomulario2(): void{
  this.forma.patchValue({
    apellido: ''
  });
}

get pass1Novalido(){
  return this.forma.get('pass1')?.invalid && this.forma.get('pass1')?.touched;
}

get pass2Novalido(){
  const pass1 = this.forma.get('pass1')?.value;
  const pass2 = this.forma.get('pass2')?.value;
  
  return this.forma.get('pass2')?.hasError('noEsIgual');
}

}