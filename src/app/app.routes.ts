import { RouterModule, Routes } from "@angular/router";
import { AppRoutingModule } from "./app-routing.module";
import { CurriculumComponent } from "./components/curriculum/curriculum.component";
import { ExperienciaComponent } from "./components/experiencia/experiencia.component";
import { NadvarComponent } from "./components/nadvar/nadvar.component";
import { EducacionComponent } from "./components/educacion/educacion.component";
import { FormularioComponent } from "./components/formulario/formulario.component";
import { Formulario2Component } from "./components/formulario2/formulario2.component";
import { AgregarComponent } from "./components/agregar/agregar.component";
import { VerComponent } from "./components/ver/ver.component";


const APP_ROUTES: Routes = [
    { path: 'curriculum', component: CurriculumComponent },
    { path: 'experiencia', component: ExperienciaComponent },
    { path: 'nadvar', component: NadvarComponent },
    { path: 'educacion', component: EducacionComponent },
    { path: 'formulario', component: FormularioComponent },
    { path: 'formulario2', component: Formulario2Component },
    { path: 'agregar/:id', component: AgregarComponent },
    { path: 'ver/:id', component: VerComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'curriculum' }
]




export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
